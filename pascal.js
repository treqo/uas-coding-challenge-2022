/**
 * Author:      Tareq Alansari
 * @param:      rowIndex
 * @return      nth row where n is rowIndex
 */
function pascal(rowIndex) {
    
    const row = [];
    
    row.push(1);
    
    if(rowIndex == 0){
        return row;
    }
    
    row.push(1); 
    
    for(let i = 2; i<=rowIndex; i++){
        for(let j = row.length-1; j>0; j--){
            row[j] = row[j] + row[j-1];
        }
        row.push(1);
    }
    
    
    return row;
    
};

//some test cases

for(let i=0; i<5; i++) {
    let test = i;
    console.log(pascal(test));
}

console.log(pascal(10)); 
